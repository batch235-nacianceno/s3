const chai = require("chai");
const { assert } = require("chai");


// import and use chai -http to allow chai to send requests to our server

const http = require("chai-http");
chai.use(http);


describe('API Test Suite for users', () => {
    it('Test API get users is running', (done) => {
        // request() method is used from chai to create an http request given to the server
        // get("/endpoint") method is used to run/access a get method route
        // end() method handles responses and errors from the http request. It has an anonymous function as an argument that receives 2 objects, the err or the reponse
        chai.request("http://localhost:4000")
        .get("/users")
        .end((err,res) => {
            // isDefined is assertion that the given data is not undefinded. It's like a shortcute ot .notEqual(typeof data, undefined)
            assert.isDefined(res);
            // done() method is used to tell chai-http when the test is done
            done();
        })
    })
    
    it('Test API get users returns an array', (done) => {
        chai.request("http://localhost:4000")
        .get("/users")
        .end((err,res) => {
            // isArray() is an assertion that the given data is an array
            // res.body contains the body of the respnse. The data sent from res.send()


            // aseert.notEqual(typeof res.body.users, "undefined")
            console.log(res.body);
            assert.isArray(res.body);
            done();
        })
    })

    it('Test API get users array frist object username is Jojo', (done) => {
        chai.request("http://localhost:4000")
        .get("/users")
        .end((err,res) => {
            // assert the the first item in array with property username is equal to Jojo
            assert.equal(res.body[0].username,"Jojo");
            done();
        })
    });
    
    it('Test API users last item is not undefined', (done) => {
        chai.request("http://localhost:4000")
        .get("/users")
        .end((err,res) => {
            console.log(res.body[2]);
            assert.notEqual(res.body[res.body.length-1], undefined);
        });
        done();
    })

    it('Test API post users returns 400 if no name', (done) => {
        // post () which is used by chai http to access a post method route
        // type() which is used to tell chai that request body is going to be stringified as a json
        // send() is used to send the request body
        chai.request("http://localhost:4000")
        .post("/users")
        .type("json")
        .send({
            age: 30,
            username: "jin92"
        })
        .end((err,res) => {
            assert.equal(res.status, 400)
            
        })
        done();
    })

    it('Test API post users returns 400 if no age', (done) => {
        // post () which is used by chai http to access a post method route
        // type() which is used to tell chai that request body is going to be stringified as a json
        // send() is used to send the request body
        chai.request("http://localhost:4000")
        .post("/users")
        .type("json")
        .send({
            name: "Jin",
            username: "jin92"
        })
        .end((err,res) => {
            assert.equal(res.status, 400)
          
        })
        done();
    });
    
});


describe('API Test Suite for artists', () => {
    it('Test API get artists returns an array', (done) => {
        chai.request("http://localhost:4000")
        .get("/artists")
        .end((err,res) => {
            
            assert.isArray(res.body);

            console.log(res.body);
           
            done();
        })
    })

    it('Test if songs properties of first object is an array', (done) => {
        chai.request("http://localhost:4000")
        .get("/artists")
        .end((err,res) => {
            
            assert.isArray(res.body[0].songs);
            done();
        })
    });

    it('Test if post endpoint encounters an error if there is no name', (done) => {
        chai.request("http://localhost:4000")
        .post("/artists")
        .type("json")
        .send({
            songs: ["Green Light","Ribs"],
            album: "Pure Heroine",
            isActive: true
        })
        .end((err,res) => {
            assert.equal(res.status, 400)
        })
        done();
        
    });

    it('Test if post endpoint encounters an error if there is no songs', (done) => {
        chai.request("http://localhost:4000")
        .post("/artists")
        .type("json")
        .send({
            name: "Lorde",
            album: "Pure Heroine",
            isActive: true
        })
        .end((err,res) => {
            assert.equal(res.status, 400)
        })
        done();
        
    });

    it('Test if post endpoint encounters an error if there is no album', (done) => {
        chai.request("http://localhost:4000")
        .post("/artists")
        .type("json")
        .send({
            name: "Lorde",
            songs: ["Green Light","Ribs"],
            isActive: true
        })
        .end((err,res) => {
            assert.equal(res.status, 400)
        })
        done();
        
    });

    it('Test if post endpoint encounters an error if isActive property is false', (done) => {
        chai.request("http://localhost:4000")
        .post("/artists")
        .type("json")
        .send({
            name: "Lorde",
            songs: ["Green Light","Ribs"],
            album: "Pure Heroine",
            isActive: true
        })
        .end((err,res) => {
            assert.equal(res.status, 400)
        })
        done();
        
    });
    
});
