const express = require("express");
// const mongoose = require("mongoose");
const app = express();

const PORT = 4000;


app.use(express.json());

let users = [
    {
        name: "Jojo Joestar",
        age: 25,
        username: "Jojo"
    },
    {
        name: "Dio Brando",
        age: 23,
        username: "Dio"
    },
    {
        name: "Jotaro Kujo",
        age: 28,
        username: "Jotaro"
    }
];

// Activity
let artists = [
    {
        name: "Taylor Swift",
        songs: [
            'Style',
            'right where you left me'
        ],
        album: "folklore",
        isActive: true
    },
    {
        name: "Phoebe Bridgers",
        songs: [
            'Garden Song',
            'Scott Street'
        ],
        album: "Stranger in the Alps",
        isActive: true
    },    {
        name: "Tyler, the Creator",
        songs: [
            'See You Again',
            'Boredom'
        ],
        album: "Flower Boy",
        isActive: true
    }

]


// [SECTION] Routes and Controllers for USERS

// send users array when endpoint is accessed
app.get("/users", (req,res) => {
    return res.send(users);
});

app.post("/users", (req,res) => {
    // add simple if statement that if the request body does not have property name, we will send message along with a 400 http status code (Bad request)
    // hasOwnProperty() returns a boolean if the property name passed exists or does not exist in the given object
    if(!req.body.hasOwnProperty("name")){
        return res.status(400).send({
            error: "Bad Request - missing required parameter NAME"
        })
    }
});

app.post("/users", (req,res) => {
    // add simple if statement that if the request body does not have property name, we will send message along with a 400 http status code (Bad request)
    // hasOwnProperty() returns a boolean if the property name passed exists or does not exist in the given object
    if(!req.body.hasOwnProperty("age")){
        return res.status(400).send({
            error: "Bad Request - missing required parameter AGE"
        })
    }
})


// Activity

app.get("/artists", (req,res) => {
    return res.send(artists);
});

app.post("/artists", (req,res) => {
    if(!req.body.hasOwnProperty("name")){
        return res.status(400).send({
            error: "Bad Request - missing required parameter NAME"
        })
    } else if (!req.body.hasOwnProperty("songs")) {
        return res.status(400).send({
            error: "Bad Request - missing required parameter SONGS"
        })
    } else if (!req.body.hasOwnProperty("album")) {
        return res.status(400).send({
            error: "Bad Request - missing required parameter ALBUM"
        })
    } else if (!req.body.isActive === false) {
        return res.status(400).send({
            error: "Bad Request - missing required parameter isActive"
        })
    }
});


app.listen(PORT, () => console.log(`Running on port ${PORT}`));
 